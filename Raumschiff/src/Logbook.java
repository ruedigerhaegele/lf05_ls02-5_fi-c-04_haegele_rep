/**
 * Klasse: Logbook (Logbuch)
 * Arbeitsauftrag LS02-5-01: Quellcode implementieren
 * <p>
 * Logbook implementiert das zentrale Logbuch-Objekt.
 * Das zentrale Logbuch enth�lt alle Eintr�ge, welche die Raumschiff-Objekte dort hinterlegen.  
 * 
 * @author R�diger Haegele (haegele@unit-m.de)
 * @version 1.0
 * @todo	Das Konzept des zentralen Logbuchs ist insofern ausbauf�hig, als dass es zur sinnvollen Nutzung 
 * 			(siehe "test.java") weitere Eigenschaften ben�tigt. So m�sste der Name des meldenden Schiffs und 
 * 			ein zeitlicher oder ereignisorienterter Zuammenhang vermerkt werden, welcher eine f�r den Konsumenten nachvollziehbare
 * 			Ausgabe erm�glicht.   
 */

import java.util.ArrayList;

public class Logbook {
	
	private ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	
	/**
	 * Konstruktor
	*/
	public Logbook () {
		
	}
	
	/**
	 * Gibt den gesamten Inhalt des Logbuchs auf STDOUT aus. 
	*/
	public void printLogbookMsgs() {
		int bckSize = broadcastKommunikator.size();
		
		if(bckSize == 0) {
			return;
		}
		
		for(int i = 0; i < bckSize; i++)  {
			System.out.println(broadcastKommunikator.get(i));
		}
	}
	
	/**
	 * Leert das Logbuch 
	*/
	public void reset() {
		broadcastKommunikator.clear();
	}

	/**
	 * @Obvious
	 */
	public ArrayList<String> getLogbook() {
		return broadcastKommunikator;
	}

	/**
	 * @Obvious
	 */
	public void setLogEntry(String text) {
		broadcastKommunikator.add(text);
	}
}
