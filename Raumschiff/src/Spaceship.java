/**
 * Klasse: Spaceship (Raumschiff)
 * Arbeitsauftrag LS02-5-01: Quellcode implementieren
 * <p>
 * Spaceship implementiert das Raumschiff-Objekt.
 * Ein Raumschiff  
 * <ul>
 * 	<li>verf�gt �ber Ladung, welche in einer Liste von Cargo-Objekten gespeichert wird.
 *  <li>schreibt Nachrichten in ein zentrales Logbuch, sofern die Ausgabe auf STDOUT nicht explizit gefordert ist.
 *  <li>feuert - sofern lagernd - die gegebene Anzahl Photonentorpedos, nachdem diese der Ladung entnommen wurden, ab.
 *  <li>feuert seine Phaserkanone ab, sofern der Energieversorgungszustand dies zul�sst.
 * </ul>
 * <p>
 * @author R�diger Haegele (haegele@unit-m.de)
 * @version 1.0
 */

import java.util.ArrayList;

public class Spaceship {

	private String name = "";
	private int shieldStatus = 100;
	private int shellStatus = 100;
	private int powerSupplyStatus = 100;
	private ArrayList<Cargo> cargoList = new ArrayList<Cargo>();
	private Logbook log;

	/**
	 * Konstruktor
	*/
	public Spaceship() {

	}

	/**
	 * parametrisierter Konstruktor
	 * @param name		- Name/Bezeichnung des Raumschiffs
	 * @param Logbook	- an Raumschiff �bergebenes, zentrales Logbuch-Objekt
	*/
	public Spaceship(String name, Logbook logbook) {
		this.setName(name);
		this.log = logbook;
	}

	/**
	 * Befehl an Raumschiff, die gegebene Anzahl Torpedos auf das angegebene Raumschiff abzufeuern.
	 * @param raumschiff	- Raumschiff-Objekt, auf welches gefeuert wird 
	 * @param numTorpedos	- Anzahl (sofern lagernd) der abzufeuernden Torpedos
	*/
	public void fireTorpedo(Spaceship raumschiff, int numTorpedos) {
		if (numTorpedos == 0) {
			System.out.println("Der Befehl 0 Torpedos abzufeuern kann nicht ausgef�hrt werden.");
		}
		int firedTorpedos = this.prepareTorpedo(numTorpedos);

		if (firedTorpedos > 0) {
			log.setLogEntry(firedTorpedos + " Photonentorpedo(s) gegen " + raumschiff.getName() + " eingesetzt.");
			raumschiff.recordHit(firedTorpedos);
		} else {
			System.out.println("Keine Photonentorpedos gefunden!");
			log.setLogEntry("-=*Click*=-");
		}
	}

	/**
	 * Befehl an Raumschiff, die Phaserkanone auf das angegebene Raumschiff abzufeuern.
	 * @param raumschiff	- Raumschiff-Objekt, auf welches gefeuert wird
	*/
	public void firePhaser(Spaceship raumschiff) {
		int powerSupplyStatus = this.getPowerSupplyStatus();

		if (powerSupplyStatus >= 50) {
			log.setLogEntry("Phaser auf " + raumschiff.getName() + " abgeschossen.");
			raumschiff.recordHit(1);
			this.setPowerSupplyStatus(powerSupplyStatus - 50);
		} else {
			log.setLogEntry("-=*Click*=-");
		}
	}

	/**
	 * Stellt den Status des Raumschiffs fest und setzt seinen (Gesundheits-)Status herab - bis hin zur Zerst�rung. 
	 * @param hitsReceived	- Anzahl der Treffer, die verarbeitet werden m�ssen
	*/
	private void recordHit(int hitsReceived) {
		int shieldStatus = this.getShieldStatus();

		log.setLogEntry("Das Schiff " + this.getName() + " wurde getroffen.");

		int shieldDamage = 50 * hitsReceived;

		// Status der Schilde herabsetzen
		if (shieldStatus >= shieldDamage) {
			this.setShieldStatus(shieldStatus - shieldDamage);

		} else if (shieldStatus < shieldDamage) {
			this.setShieldStatus(0);

			// Schild f�ngt potentiell einen Teil des Schadens ab
			int damage = shieldDamage - shieldStatus;

			// Status der H�lle und der Energieversorgung herabsetzen
			int shellStatus = this.getShellStatus();
			int powerSupplyStatus = this.getPowerSupplyStatus();

			boolean shipDestroyed = false;

			if (shellStatus > damage) {
				shellStatus -= damage;
				this.setShellStatus(shellStatus);
			} else if (shellStatus <= damage) {
				this.setShellStatus(0);
				shipDestroyed = true;
			}

			if (powerSupplyStatus > damage) {
				powerSupplyStatus -= damage;
				this.setPowerSupplyStatus(powerSupplyStatus);
			} else if (powerSupplyStatus <= damage) {
				this.setPowerSupplyStatus(0);
			}

			if (shipDestroyed) {
				log.setLogEntry("Die Lebenserhaltungssysteme der " + this.getName()
						+ " wurden zerst�rt, das Schiff wurde vernichtet.");
			}

		}
	}

	/**
	 * Pr�ft den Lagerbestand an Torpedos, die f�r den Abschuss vom Lager genommen werden, und setzt ihn entsprechend.  
	 * @param numTorpedos	- Anzahl der zu ladenden Torpedos
	 * @return 				- Anzahl der geladenen Torpedos
	*/
	private int prepareTorpedo(int numTorpedos) {
		String type = "torpedo";
		int loadedTorpedos = -1;
		int stockAmount = this.getCargoItemAmount(type);

		if (numTorpedos < 1 || stockAmount < 1) {
			return 0;
		}

		if (stockAmount >= numTorpedos) {
			loadedTorpedos = numTorpedos;
		} else if (stockAmount < numTorpedos) {
			loadedTorpedos = stockAmount;
		}

		this.setCargoItemAmount(type, (stockAmount - loadedTorpedos));

		return loadedTorpedos;
	}

	/**
	 * F�gt die gegebene Anzahl eines Objekts der Ladungsliste/dem Lager hinzu.  
	 * @param type		- Typ des einzulagernden Objekts (z.B. "torpedo")
	 * @param amount	- Menge des einzulagernden Objekts
	*/
	public void addLadung(String type, int amount) {

		if (!cargoList.isEmpty()) {
			boolean stockUpdated = false;

			for (Cargo lagerItem : cargoList) {
				if (lagerItem.getType() == type) {
					lagerItem.setAmount(lagerItem.getAmount() + amount);
					stockUpdated = true;
					break;
				}
			}

			if (!stockUpdated) {
				Cargo ladung = new Cargo(type, amount);
				this.cargoList.add(ladung);
			}
		} else {
			Cargo ladung = new Cargo(type, amount);
			this.cargoList.add(ladung);
		}
	}

	/**
	 * Ermittelt die Lagermenge des gegebenen Objekts auf Lager.
	 * @param type		- Typ des lagernden Objekts (z.B. "torpedo"), dessen Lagermenge man erfahren m�chte
	 * @return			- Lagerbestand des angegebenen Objekts oder "-1" wenn das Objekt nicht gefunden werden konnte oder "-2" wenn die Lagerliste komplett leer ist. 
	*/
	public int getCargoItemAmount(String type) {
		int retVal = 0;

		if (!this.cargoList.isEmpty()) {
			boolean cargoItemFound = false;

			for (Cargo lagerItem : this.cargoList) {
				if (lagerItem.getType() == type) {
					retVal = lagerItem.getAmount();
					cargoItemFound = true;
				}
			}

			if (!cargoItemFound) {
				retVal = -1;
			}
		} else {
			retVal = -2;
		}

		return retVal;
	}

	/**
	 * Setzt die Lagermenge des gegebenen Objekts.   
	 * @param type		- Typ des lagernden Objekts (z.B. "torpedo"), dessen Lagermenge man setzen m�chte.
	 * @param amount	- Zu setzende Lagermenge des gegebenen Objekts
	 * @return			Entweder erfolgreich gesetzte Lagermenge oder "-1" bei Misserfolg
	 * @todo			Sinn (Nutzen) des R�ckgabewerts "-1" hinterfragen und ggf. refakturieren
	*/
	public int setCargoItemAmount(String type, int amount) {
		boolean stockUpdated = false;

		if (!this.cargoList.isEmpty()) {
			
			for (int i = 0; i < this.cargoList.size(); i++) {
				Cargo listItem = this.cargoList.get(i);

				if (listItem.getType() == type) {
					if (amount == 0) {
						this.cargoList.remove(i);
					} else if (amount > 0) {
						listItem.setAmount(amount);
						stockUpdated = true;
						return amount;
					}
				}
			}
		}

		if (!stockUpdated && amount > 0) {
			this.addLadung(type, amount);
			return amount;
		}

		return -1;
	}

	/**
	 * @Obvious
	 */
	ArrayList<Cargo> getCargoList() {
		return this.cargoList;
	}

	/**
	 * @Obvious
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * @Obvious
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @Obvious
	 */
	public int getPowerSupplyStatus() {
		return powerSupplyStatus;
	}

	/**
	 * @Obvious
	 */
	public void setPowerSupplyStatus(int powerSupplyStatus) {
		this.powerSupplyStatus = powerSupplyStatus;
	}

	/**
	 * @Obvious
	 */
	public int getShieldStatus() {
		return shieldStatus;
	}

	/**
	 * @Obvious
	 */
	public void setShieldStatus(int shieldStatus) {
		this.shieldStatus = shieldStatus;
	}

	/**
	 * @Obvious
	 */
	public int getShellStatus() {
		return shellStatus;
	}

	/**
	 * @Obvious
	 */
	public void setShellStatus(int shellStatus) {
		this.shellStatus = shellStatus;
	}
}
