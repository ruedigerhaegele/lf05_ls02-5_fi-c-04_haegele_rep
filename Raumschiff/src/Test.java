/**
 * Klasse: Test
 * Arbeitsauftrag LS02-5-01: Quellcode implementieren
 * <p>
 * In der Klasse Test wird ein Programmablauf erzeugt. D.h. die in den anderen Klassen entwickelten
 * Funktionen werden ausgef�hrt und das Ergebnis - erg�nzt durch den User unterst�tzende Kommentare - wird ausgegeben.
 * 
 * @author R�diger Haegele (haegele@unit-m.de)
 * @version 1.0
 */

import java.util.ArrayList;

public class Test {

	public static void main(String[] args) {
		// Logbuch erzeugen
		Logbook logbook = new Logbook();

		// Enterprise erzeugen
		Spaceship enterprise = new Spaceship("Enterprise", logbook);
		enterprise.addLadung("torpedo", 4);
		enterprise.addLadung("android", 4);
		
		// Borg-Schiff erzeugen
		Spaceship borg = new Spaceship("Borg", logbook);
		borg.addLadung("torpedo", 2);
		borg.addLadung("android", 4);
		
		// Arbeitsauftrag LS02-5-01: Den Zustand des Raumschiffes ausgeben
		// Alle Zust�nde (Attributwerte) des Raumschiffes auf der Konsole mit entsprechenden Namen ausgeben.
		System.out.println("1. -----------------------------------------------------------------");
		printShieldStatus(enterprise);
		printShellStatus(enterprise);
		printPowerStatus(enterprise);
		
		
		// Arbeitsauftrag LS02-5-01: Ladungsverzeichnis ausgeben
		// Alle Ladungen eines Raumschiffes auf der Konsole mit Ladungsbezeichnung und Menge ausgeben.
		System.out.println("2. -----------------------------------------------------------------");
		System.out.println("Ladungsverzeichnisse der Schiffe:");
		printLadungsverzeichnis(enterprise);

		System.out.println("--");
		
		printLadungsverzeichnis(borg);
		
		// Arbeitsauftrag LS02-5-01: Photonentorpedos abschie�en
		// Gibt es keine Torpedos, so wird als Nachricht an Alle -=*Click*=- ausgegeben. (Anforderungen �berschneiden sich an dieser Stelle: Click vs. "Keine Photonentorpedos gefunden!" aus 6.
		// Ansonsten wird die Torpedoanzahl um eins reduziert und die Nachricht an Alle Photonentorpedo abgeschossen gesendet. 
		// Au�erdem wird die Methode Treffer aufgerufen.
		System.out.println("3. -----------------------------------------------------------------");
		logbook.reset();

		printShieldStatus(enterprise);
		printShellStatus(enterprise);
		borg.fireTorpedo(enterprise, 1);
		logbook.printLogbookMsgs();
		printShieldStatus(enterprise);
		printShellStatus(enterprise);
		
		System.out.println("--");
		logbook.reset();
		System.out.println("Vorgabe: Borg-Ladung enth�lt keine Torpedos.");
		borg.setCargoItemAmount("torpedo", 0);
		printLadungsverzeichnis(borg);
		System.out.println("Schussversuch von Borg auf Enterprise mit Torpedo:");
		borg.fireTorpedo(enterprise, 1);
		logbook.printLogbookMsgs();


		// Arbeitsauftrag LS02-5-01: Phaserkanone abschie�en
		// Ist die Energieversorgung kleiner als 50%, so wird als Nachricht an Alle -=*Click*=- ausgegeben.
		// Ansonsten wird die Energieversorgung um 50% reduziert und die Nachricht an Alle �Phaserkanone abgeschossen� gesendet. Au�erdem wird die Methode Treffer aufgerufen.
		// && Treffer vermerken 
		// Die Nachricht [Schiffsname] wurde getroffen! wird in der Konsole ausgegeben. 
		// && Nachrichten an Alle
		// Die Nachricht wird dem broadcastKommunikator hinzugef�gt
		// && Logbuch Eintr�ge zur�ckgeben
		// "Gibt den broadcastKommunikator zur�ck" => ich habe hier die im Logbuch d.h. im Array broadcastKommunikator gespeicherten Eintr�ge ausgegeben. 
		// Ansonsten kann man sich �ber Logbook->getLogbook() auch das Array liefern lassen. 
		
		System.out.println("4. -----------------------------------------------------------------");
		logbook.reset();
		
		System.out.println("Versuch,den BorgPhaser trotz Energieversorgungsstatus 40 einzusetzen:");
		borg.setPowerSupplyStatus(40); // auf Testwert <50 gesetzt
		borg.firePhaser(enterprise);
		logbook.printLogbookMsgs();
		
		// ...Schiffe wiederherstellen
		borg.setPowerSupplyStatus(100);
		enterprise.setShieldStatus(100);
		
		System.out.println("--");
		logbook.reset();

		System.out.println("Schiffe wiederherstellen");
		printPowerStatus(borg);
		borg.firePhaser(enterprise);
		logbook.printLogbookMsgs();
		printPowerStatus(borg);
		printShieldStatus(enterprise);

		// ...Schiffe wiederherstellen
		borg.setPowerSupplyStatus(100);
		enterprise.setShieldStatus(100);
		
		// Arbeitsauftrag LS02-5-01: Treffer vermerken
		// Die Schilde des getroffenen Raumschiffs werden um 50% geschw�cht.
		// Sollte anschlie�end die Schilde vollst�ndig zerst�rt worden sein, so wird der Zustand der H�lle und der Energieversorgung jeweils um 50% abgebaut.
		// Sollte danach der Zustand der H�lle auf 0% absinken, so sind die Lebenserhaltungssysteme vollst�ndig zerst�rt und es wird eine Nachricht an Alle 
		// ausgegeben, dass die Lebenserhaltungssysteme vernichtet worden sind.
		// && Ladungsverzeichnis aufr�umen
		// Wenn die Menge einer Ladung 0 ist, dann wird das Objekt Ladung aus der Liste entfernt.
		
		System.out.println("5. -----------------------------------------------------------------");
		
		System.out.println("Schiffe wiederherstellen");
		printShieldStatus(enterprise);
		printShellStatus(enterprise);
		printPowerStatus(enterprise);
		printPowerStatus(borg);
		borg.setCargoItemAmount("torpedo", 2);
		printLadungsverzeichnis(borg);
		
		System.out.println("a --");
		logbook.reset();

		borg.fireTorpedo(enterprise, 1);
		logbook.printLogbookMsgs();
		printShieldStatus(enterprise);
		printShellStatus(enterprise);
		printPowerStatus(enterprise);
		printLadungsverzeichnis(borg);

		System.out.println("b --");
		logbook.reset();

		borg.fireTorpedo(enterprise, 1);
		logbook.printLogbookMsgs();
		printShieldStatus(enterprise);
		printShellStatus(enterprise);
		printPowerStatus(enterprise);
		printLadungsverzeichnis(borg);
		System.out.println("Dem aufmerksamen Leser entgeht nicht, dass sich im Lager keine Torpedos mehr befinden.");
		
		System.out.println("c --");
		logbook.reset();

		borg.firePhaser(enterprise);
		logbook.printLogbookMsgs();
		printShieldStatus(enterprise);
		printShellStatus(enterprise);
		printPowerStatus(enterprise);
		printPowerStatus(borg);
		printLadungsverzeichnis(borg);

		System.out.println("d --");
		logbook.reset();

		borg.firePhaser(enterprise);
		logbook.printLogbookMsgs();
		printShieldStatus(enterprise);
		printShellStatus(enterprise);
		printPowerStatus(enterprise);
		printPowerStatus(borg);
		printLadungsverzeichnis(borg);

		// Arbeitsauftrag LS02-5-01: Ladung �Photonentorpedos� einsetzen
		// Gibt es keine Ladung Photonentorpedos auf dem Schiff, wird als Nachricht Keine Photonentorpedos gefunden! in der Konsole ausgegeben und die Nachricht an alle -=*Click*=- ausgegeben.
		// Ist die Anzahl der einzusetzenden Photonentorpedos gr��er als die Menge der tats�chlich Vorhandenen, so werden alle vorhandenen Photonentorpedos eingesetzt.
		// Ansonsten wird die Ladungsmenge Photonentorpedos �ber die Setter Methode vermindert und die Anzahl der Photonentorpedo im Raumschiff erh�ht.
		// Konnten Photonentorpedos eingesetzt werden, so wird die Meldung [X] Photonentorpedo(s) eingesetzt auf der Konsole ausgegeben. [X] durch die Anzahl ersetzen.
		System.out.println("6. -----------------------------------------------------------------");
		logbook.reset();
		System.out.println("Schiffe wiederherstellen");
		
		// ...Schiffe wiederherstellen
		borg.setPowerSupplyStatus(100);
		borg.addLadung("torpedo", 6);
		
		printLadungsverzeichnis(borg);

		enterprise.setShieldStatus(100);
		enterprise.setShellStatus(100);
		enterprise.setPowerSupplyStatus(100);

		printShieldStatus(enterprise);
		printShellStatus(enterprise);
		printPowerStatus(enterprise);

		borg.fireTorpedo(enterprise, 4);
		logbook.printLogbookMsgs();

		printShieldStatus(enterprise);
		printShellStatus(enterprise);
		printPowerStatus(enterprise);
		
		printLadungsverzeichnis(borg);

		System.out.println("--");
		System.out.println("Schiffe wiederherstellen");
		logbook.reset();
		
		printLadungsverzeichnis(borg);

		enterprise.setShieldStatus(100);
		enterprise.setShellStatus(100);
		enterprise.setPowerSupplyStatus(100);

		printShieldStatus(enterprise);
		printShellStatus(enterprise);
		printPowerStatus(enterprise);

		borg.fireTorpedo(enterprise, 2);
		logbook.printLogbookMsgs();

		printShieldStatus(enterprise);
		printShellStatus(enterprise);
		printPowerStatus(enterprise);
		
		printLadungsverzeichnis(borg);

	}
	
	/**
	 * Gibt das Ladungsverzeichnis des gegebenen Raumschiffs auf STDOUT aus.
	 * @param raumschiff	Gegebenes Raumschiff
	 */
	static void printLadungsverzeichnis(Spaceship raumschiff) {
	
		ArrayList<Cargo> ladungsListe = raumschiff.getCargoList();
		
		for(Cargo ladung: ladungsListe) {
			System.out.print(raumschiff.getName() + " ");
			System.out.print(ladung.getType() + ": ");
			System.out.print(ladung.getAmount());
			System.out.print("\n");
		}
	}
	
	/**
	 * Gibt den Schild-Status des gegebenen Raumschiffs auf STDOUT aus.
	 * @param raumschiff	Gegebenes Raumschiff
	 */
	static void printShieldStatus(Spaceship raumschiff) {
		System.out.println("Der Schild-Status von " + raumschiff.getName() + " liegt bei " + raumschiff.getShieldStatus() + " Prozent.");
	}
	
	/**
	 * Gibt den H�llen-Status des gegebenen Raumschiffs auf STDOUT aus.
	 * @param raumschiff	Gegebenes Raumschiff
	 */
	static void printShellStatus(Spaceship raumschiff) {
		System.out.println("Der H�lle-Status von " + raumschiff.getName() + " liegt bei " + raumschiff.getShellStatus() + " Prozent.");
	}
	
	/**
	 * Gibt den Energie-Status des gegebenen Raumschiffs auf STDOUT aus.
	 * @param raumschiff	Gegebenes Raumschiff
	 */
	static void printPowerStatus(Spaceship raumschiff) {
		System.out.println("Der Energie-Status von " + raumschiff.getName() + " liegt bei " + raumschiff.getPowerSupplyStatus() + " Prozent.");
	}
}
