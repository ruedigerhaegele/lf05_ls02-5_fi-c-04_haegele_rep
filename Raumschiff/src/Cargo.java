/**
 * Klasse: Cargo (Ladung)
 * Arbeitsauftrag LS02-5-01: Quellcode implementieren
 * <p>
 * Cargo implementiert das Ladungs-Objekt, welches in der Ladungsliste eines Raumschiffs gespeichert werden kann.
 * 
 * @author R�diger Haegele (haegele@unit-m.de)
 * @version 1.0
 */

public class Cargo {
	private String type = "";
	private int amount = 0;

	/**
	 * Konstruktor
	*/
	public Cargo() {
	}

	/**
	 * parametrisierter Konstruktor
	 * @param type		- Der Typ wird durch die Angabe eines einfachen Strings festgelegt.
	 * @param amount	- Anzahl, in welcher das Ladungsobjekt in einem Raumschiff vorhanden ist.
	*/
	public Cargo(String type, int amount) {
		this.setType(type);
		this.setAmount(amount);
	}

	/**
	 * @Obvious
	 */
	public String getType() {
		return type;
	}

	/**
	 * @Obvious
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @Obvious
	 */
	public int getAmount() {
		return amount;
	}

	/**
	 * @Obvious
	 */
	public void setAmount(int amount) {
		this.amount = amount;
	}
}
